


let inNum1;
let inNum2;
let inOperation;

inNum1 = parseInt(prompt("Enter number 1"));
inNum2 = parseInt(prompt("Enter number 2"));
inOperation = prompt("Enter operation: +, -, *, /");

if (isNaN(inNum1)) {
    do {
        if (isNaN(inNum1))
            inNum1 = parseInt(prompt("Number 1 was entered incorrectly! Try again!"));
    } while (isNaN(inNum1))
}

if (isNaN(inNum2)) {
    do {
        if (isNaN(inNum2))
            inNum2 = parseInt(prompt("Number 2 was entered incorrectly! Try again!"));
    } while (isNaN(inNum2))
}

if (inOperation !== "+" && inOperation !== "-" && inOperation !== "*" && inOperation !== "/") {
    inOperation = prompt('Operation was entered incorrectly! Try again!')

} while (inOperation !== "+" && inOperation !== "-" && inOperation !== "*" && inOperation !== "/") ;


console.log(process(inNum1, inNum2, inOperation));


function process(num1, num2, operation) {

    switch (operation){
        case "+":
            return num1 + num2;
            break;
        case "-":
            return num1 - num2;
            break;
        case "*":
            return num1 * num2;
            break;
        case "/":
            return num1 / num2;
            break;
    }

}


//функции нужны для избежания дублирования кода в разных местах
//аргументы это значения\параметры функции, которые она использует для своей работы